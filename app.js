const express = require("express");
const app = express();

app.listen(8080);

app.use(express.static("public"));


app.get("/", function(req, res){
    res.send("Hello world !");
});

app.get("/about", function(req, res){
    res.send("Welcome About page !");
});

app.get("/products", function(req, res){
    res.json([
        {id: 1, name: "core i3"},
        {id: 2, name: "core i5"},
        {id: 3, name: "core i7"},
        {id: 4, name: "core i9"}
    ]);
});

app.get("/paok", (req, res) => {
    res.sendFile(__dirname + '/files/paok.html');
});
